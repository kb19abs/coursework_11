# Coursework 11 Team 
** 1. Abhishek Bhandare  
2. Atharva Dhumal  
3. Himanshu Dhumal  
4. Kedar Bibave  
5. Aniket Indulkar ** 

## Company Acquisitions Data
This dataset contains the rundown of acquisitions made by the accompanying organizations:  
1.Google  
2.Microsoft  
3.Facebook  
4.Apple  
5.Yahoo  
6.Twitter  
7.IBM  

### Content
The qualities incorporate the date, year, month of the obtaining, name of the organization gained,
esteem  
or the expense of procurement, business use-instance of the securing,and the nation from which
the securing  
was made, the determined item utilized by each organization.

Which organization makes the acquisitions rapidly.  

The pattern of business use-cases among the procured organizations consistently.  

Guage for up and coming a long time as far as acquisitions.  

## Research questions
**1. Reason for acquiring other companies is to make own product better with acquired companies platform.  
2. Is the trend of acquiring other companies going down or small company started realizing there potential?  
3. Is the USA a better place than any other nations for small companies to get acquired by top companies?  **

## Conclusion
Conclusion for RQ1, According to data we have in data set we conclude that some companies look for enhancing  
and deriving new product for own company by acquired company’s platform. But some companies do not use acquire  
platform at all. With limited data we have in data set we conclude that acquisitions are completely dependent  
on mind-sets of company. Analysis of RQ2 summarizes that USA is a developed country which provides better  
infrastructure, funding options and healthy competition for start-ups. Hence USA is best place for start-ups  
and to get acquired by big companies. RQ3: In 2013 & 2014 most of the acquisition deals took place. Maximum  
start-ups doesn’t know their ultimate goal, so end with merger and acquisition.

## References
Dataset : Kaggle.com. (2019). Company Acquisitions Data. [online] Available at:   
  https://www.kaggle.com/shivamb/company-acquisitions-7-top-companies [Accessed 20 Dec. 2019].  
  En.wikipedia.org. (2019). List of mergers and acquisitions by Alphabet. [online] Available at:  
  https://en.wikipedia.org/wiki/List_of_mergers_and_acquisitions_by_Alphabet [Accessed 20 Dec. 2019].  
  Svb.com. (2019). [online] Available at:   
  https://www.svb.com/globalassets/library/uploadedfiles/content/trends_and_insights/reports/startup_outlook_report/us/svb-suo-us-report-2019.pdf [Accessed 20 Dec. 2019].  
  Global Finance Magazine. (2019). Global Finance Magazine - Largest M&A Deals 2013. [online]  
  Available at: https://www.gfmag.com/global-data/economic-data/largest-maa-deals [Accessed 20 Dec. 2019].  
  Virgin. (2019). Is America the most entrepreneurial country?. [online]  
  Available at: https://www.virgin.com/entrepreneur/america-most-entrepreneurial-country [Accessed 20 Dec. 2019].  